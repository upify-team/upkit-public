# UPKit

[![CI Status](http://img.shields.io/travis/marpal/UPKit.svg?style=flat)](https://travis-ci.org/marpal/UPKit)
[![Version](https://img.shields.io/cocoapods/v/UPKit.svg?style=flat)](http://cocoapods.org/pods/UPKit)
[![License](https://img.shields.io/cocoapods/l/UPKit.svg?style=flat)](http://cocoapods.org/pods/UPKit)
[![Platform](https://img.shields.io/cocoapods/p/UPKit.svg?style=flat)](http://cocoapods.org/pods/UPKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

UPKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "UPKit"
```

## Author

marpal, diego.pais29@gmail.com

## License

UPKit is available under the MIT license. See the LICENSE file for more info.
