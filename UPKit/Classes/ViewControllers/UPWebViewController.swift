//
//  UPWebViewController.swift
//
//  Created by Diego Pais on 9/4/18.
//

import UIKit
import WebKit

open class UPWebViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
    
    open var webUrl: URL? {
        didSet {
            if isViewLoaded {
                loadUrl()
            }
        }
    }
    
    open var progressTintColor: UIColor = UIColor.blue {
        didSet {
            progressView.progressTintColor = progressTintColor
        }
    }
    
    open var closeImageName = "icClose"
    
    open var actionsBarBackgroundColor: UIColor = .white
    open var actionsBarTintColor: UIColor = .blue
    open var actionBarEnabled = false {
        didSet {
            if isViewLoaded {
                webViewBottomConstraint.isActive = !actionBarEnabled
                actionsBarTopConstraint.isActive = actionBarEnabled
                actionsBarView.isHidden = !actionBarEnabled
            }
        }
    }
    
    private(set) var webView = WKWebView()
    private var webViewBottomConstraint: NSLayoutConstraint!
    
    private(set) var actionsBarView = UIView()
    private var actionsBarHeightConstraint: NSLayoutConstraint!
    private var actionsBarTopConstraint: NSLayoutConstraint!
    private var actionsBarHeight: CGFloat = 44
    
    private var backButton = UIButton(type: .system)
    private var forwardButton = UIButton(type: .system)
    
    private var progressView = UIProgressView(progressViewStyle: .default)
    
    
    deinit {
        webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress))
        webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.url))
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.url), options: .new, context: nil)
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        
        if navigationController?.viewControllers.count ?? 0 <= 1 {
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: closeImageName), landscapeImagePhone: nil, style: .plain, target: self, action: #selector(handleCloseAction(_:)))
        }
        
        setupUI()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        loadUrl()
    }
    
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var bottomInset: CGFloat = 0
        if #available(iOS 11.0, *) {
            bottomInset = view.safeAreaInsets.bottom
        }
        
        actionsBarHeightConstraint.constant = actionsBarHeight + bottomInset
    }
    
    //    MARK: - Actions
    
    @objc func handleCloseAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func handleBackAction(_ sender: Any) {
        
        if webView.canGoBack {
            webView.goBack()
        }
    }
    
    @objc func handleForwardAction(_ sender: Any) {
        
        if webView.canGoForward {
            webView.goForward()
        }
    }
    
    open func setupUI() {
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(webView)
        
        view.sendSubviewToBack(webView)
        
        webView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        webView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        webView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        webViewBottomConstraint = webView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        
        webView.backgroundColor = .white
        webView.scrollView.backgroundColor = .white
        
        webView.navigationDelegate = self
        webView.uiDelegate = self
        
        view.addSubview(progressView)
        
        progressView.translatesAutoresizingMaskIntoConstraints = false
        progressView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        progressView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        if navigationController?.navigationBar.isTranslucent == true {
            let statusBarHeight = UIApplication.shared.statusBarFrame.height
            progressView.topAnchor.constraint(equalTo: topLayoutGuide.topAnchor, constant: statusBarHeight + 44).isActive = true
        }else {
            progressView.topAnchor.constraint(equalTo: topLayoutGuide.topAnchor, constant: 0).isActive = true
        }
        
        progressView.trackTintColor = .clear
        progressView.progressTintColor = progressTintColor
        
        setupActionsBar()
        
        webViewBottomConstraint.isActive = !actionBarEnabled
        actionsBarTopConstraint.isActive = actionBarEnabled
        actionsBarView.isHidden = !actionBarEnabled
    }
    
    private func loadUrl() {
        if let url = webUrl {
            webView.load(URLRequest(url: url))
        }
    }
    
    open override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(WKWebView.estimatedProgress) {
            progressView.setProgress(Float(webView.estimatedProgress), animated: true)
            progressView.isHidden = webView.estimatedProgress == 1
        }
        
        if keyPath == #keyPath(WKWebView.url) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.backButton.isEnabled = self.webView.canGoBack
                self.forwardButton.isEnabled = self.webView.canGoForward
            }
        }
    }
    
    //    MARK: WKNavigationDelegate
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        backButton.isEnabled = webView.canGoBack
        forwardButton.isEnabled = webView.canGoForward
    }
    
    //    MARK: WKUIDelegate
    
    public func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if (navigationAction.targetFrame == nil) {
            let popup = WKWebView(frame: self.view.bounds, configuration: configuration)
            popup.uiDelegate = self
            self.view.addSubview(popup)
            return popup
        }
        return nil;
    }
    
    public func webViewDidClose(_ webView: WKWebView) {
        webView.removeFromSuperview()
    }
    
    //    MARK: - Private

    private func setupActionsBar() {
        
        actionsBarView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(actionsBarView)
        
        NSLayoutConstraint.activate([
            actionsBarView.leftAnchor.constraint(equalTo: view.leftAnchor),
            actionsBarView.rightAnchor.constraint(equalTo: view.rightAnchor),
            actionsBarView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            ])
        
        actionsBarTopConstraint = actionsBarView.topAnchor.constraint(equalTo: webView.bottomAnchor)
        actionsBarHeightConstraint = actionsBarView.heightAnchor.constraint(equalToConstant: actionsBarHeight)
        actionsBarHeightConstraint.isActive = true
        
        actionsBarView.backgroundColor = actionsBarBackgroundColor
        
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.setImage(UIImage(named: "icBack")?.withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.tintColor = actionsBarTintColor
        backButton.addTarget(self, action: #selector(handleBackAction(_:)), for: .touchUpInside)
        backButton.isEnabled = false
        backButton.contentEdgeInsets = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        actionsBarView.addSubview(backButton)
        
        NSLayoutConstraint.activate([
            backButton.leftAnchor.constraint(equalTo: actionsBarView.leftAnchor, constant: 0),
            backButton.centerYAnchor.constraint(equalTo: actionsBarView.topAnchor, constant: actionsBarHeight / 2)
            ])
        
        forwardButton.translatesAutoresizingMaskIntoConstraints = false
        forwardButton.setImage(UIImage(named: "icForward")?.withRenderingMode(.alwaysTemplate), for: .normal)
        forwardButton.tintColor = actionsBarTintColor
        forwardButton.addTarget(self, action: #selector(handleForwardAction(_:)), for: .touchUpInside)
        forwardButton.isEnabled = false
        forwardButton.contentEdgeInsets = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        actionsBarView.addSubview(forwardButton)
        
        NSLayoutConstraint.activate([
            forwardButton.rightAnchor.constraint(equalTo: actionsBarView.rightAnchor, constant: 0),
            forwardButton.centerYAnchor.constraint(equalTo: actionsBarView.topAnchor, constant: actionsBarHeight / 2)
            ])
    }
    
}
