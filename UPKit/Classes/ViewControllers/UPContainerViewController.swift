//
//  UPContainerViewController
//
//  Created by Diego Pais on 6/18/18.
//

import UIKit

public class UPContainerViewController: UIViewController {
    
    public var animationOptions: UIView.AnimationOptions = .transitionCrossDissolve
    public var animationDuration: Double = 0.5
    
    private(set) var activeViewController: UIViewController?
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        setup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    //    MARK: - Public
    
    public func set(viewController: UIViewController, animated: Bool, onCompletion: (() -> ())?) {
        
        guard viewController != activeViewController else {
            onCompletion?()
            return
        }
        
        //Ensure the view is loaded
        _ = self.view
        
        addChild(viewController)
        if let activeViewController = activeViewController {
            view.insertSubview(viewController.view, belowSubview: activeViewController.view)
        }else {
            view.addSubview(viewController.view)
        }
        
        viewController.didMove(toParent: self)
        
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        viewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        viewController.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        viewController.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        viewController.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        guard let activeViewController = activeViewController else {
            self.activeViewController = viewController
            onCompletion?()
            return
        }
        
        UIView.transition(from: activeViewController.view, to: viewController.view, duration: animated ? animationDuration : 0.0, options: animationOptions) { (_) in
            
            let oldViewController = activeViewController
            self.activeViewController = viewController
            
            if self.activeViewController != oldViewController {
                oldViewController.willMove(toParent: nil)
                oldViewController.view.removeFromSuperview()
                oldViewController.removeFromParent()
            }
            onCompletion?()
        }
    }
    
    //    MARK: - Private
    
    private func setup() {
        
    }
}
