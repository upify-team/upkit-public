//
//  UIApplication+Utils.swift
//  Alamofire
//
//  Created by Diego Pais on 9/4/18.
//

import Foundation

extension UIApplication {
    
    open var versionNumber: String {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }
    
    open var buildNumber: String {
        return Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
    }
}
