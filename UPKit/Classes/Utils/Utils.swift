
import UIKit
import MapKit
import CoreLocation

open class Utils {
    
    public static func showAlert(withTitle title: String, message: String, fromViewController viewController: UIViewController, actionTitle: String? = nil, onCompletion: (() -> Void)? = nil) {

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction((UIAlertAction(title: actionTitle ?? "Ok", style: .default, handler: { (_) in
            onCompletion?()
        })))
        
        viewController.present(alert, animated: true, completion: nil)
    }
    
    public static func topViewController() -> UIViewController? {
                
        if var topViewController = UIApplication.shared.keyWindow?.rootViewController {
            
            while let presentedVC = topViewController.presentedViewController {
                topViewController = presentedVC
            }
            return topViewController
        }
        return nil
    }
    
    public static func rootViewController() -> UIViewController? {
       return UIApplication.shared.keyWindow?.rootViewController
    }
    
    public static func call(phoneNumber: String) {
        
        let filtredUnicodeScalars = "1\(phoneNumber)".unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        let formattedPhoneNumber = String(String.UnicodeScalarView(filtredUnicodeScalars))
        
        if let url = URL(string: "tel://\(formattedPhoneNumber)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @available(iOS 10.0, *)
    public static func openLocationInMaps(location: CLLocationCoordinate2D, name: String) {
        
        // TODO: Allow to choose between apple maps and google maps if installed, and allow to set one of them as default
        let placemark = MKPlacemark(coordinate: location)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = name
        mapItem.openInMaps(launchOptions: [:])
    }
}
